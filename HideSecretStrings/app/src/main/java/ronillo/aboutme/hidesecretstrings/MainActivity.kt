package ronillo.aboutme.hidesecretstrings

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate


class MainActivity : AppCompatActivity() {

    private val nativeLib = NativeLib()

    @Suppress("DEPRECATION")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textView = findViewById<TextView>(R.id.display_text)

        var info: PackageInfo? = null
        try {
            info = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        if (null != info && info.signatures.isNotEmpty()) {
            val rawCertJava = info.signatures[0].toByteArray()
            val rawCertNative: ByteArray = nativeLib.getCertificateSignature()
            val str = """
                From Java:
                ${getInfoFromBytes(rawCertJava)}From native:
                ${getInfoFromBytes(rawCertNative)}
                
                Api Endpoint = ${nativeLib.getApiEndpoint()}
                Api Key = ${nativeLib.getApiKey()}
                Api Secret = ${nativeLib.getApiSecret()}
                """.trimIndent()
            textView.text = str
        } else {
            textView.text = "No data"
        }
    }

    private fun getInfoFromBytes(bytes: ByteArray?): String? {
        if (null == bytes) {
            return "null"
        }

        /*
         * Get the X.509 certificate.
         */
        val certStream: InputStream = ByteArrayInputStream(bytes)
        val sb = StringBuilder()
        try {
            val certFactory = CertificateFactory.getInstance("X509")
            val x509Cert = certFactory.generateCertificate(certStream) as X509Certificate
            sb.append("Certificate subject: ").append(x509Cert.subjectDN).append("\n")
            sb.append("Certificate issuer: ").append(x509Cert.issuerDN).append("\n")
            sb.append("Certificate serial number: ").append(x509Cert.serialNumber).append("\n")
            var md: MessageDigest
            try {
                md = MessageDigest.getInstance("MD5")
                md.update(bytes)
                var byteArray = md.digest()
                //String hash_key = new String(Base64.encode(md.digest(), 0));
                sb.append("MD5: ").append(bytesToString(byteArray)).append("\n")
                md.reset()
                md = MessageDigest.getInstance("SHA")
                md.update(bytes)
                byteArray = md.digest()
                //String hash_key = new String(Base64.encode(md.digest(), 0));
                sb.append("SHA1: ").append(bytesToString(byteArray)).append("\n")
                md.reset()
                md = MessageDigest.getInstance("SHA256")
                md.update(bytes)
                byteArray = md.digest()
                sb.append("SHA256: ").append(bytesToString(byteArray)).append("\n")
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
            }
            sb.append("\n")
        } catch (e: CertificateException) {
            // e.printStackTrace();
        }
        return sb.toString()
    }

    private fun bytesToString(bytes: ByteArray): String? {
        val md5StrBuff = StringBuilder()
        for (i in bytes.indices) {
            if (Integer.toHexString(0xFF and bytes[i].toInt()).length == 1) {
                md5StrBuff.append("0").append(Integer.toHexString(0xFF and bytes[i].toInt()))
            } else {
                md5StrBuff.append(Integer.toHexString(0xFF and bytes[i].toInt()))
            }
            if (bytes.size - 1 != i) {
                md5StrBuff.append(":")
            }
        }
        return md5StrBuff.toString()
    }
}
