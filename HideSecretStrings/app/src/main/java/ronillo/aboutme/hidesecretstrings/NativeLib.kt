package ronillo.aboutme.hidesecretstrings

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class NativeLib {

    external fun getApiEndpoint() : String

    external fun getApiKey() : String

    external fun getApiSecret() : String

    external fun getCertificateSignature() : ByteArray

    fun getShaSignature() : String {
        val sb = StringBuilder()
        val bytes = getCertificateSignature()
        try {
            val md = MessageDigest.getInstance("SHA")
            md.update(bytes)
            val byteArray = md.digest()
            sb.append(bytesToString(byteArray))
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        return sb.toString().toUpperCase()
    }

    private fun bytesToString(bytes: ByteArray): String? {
        val md5StrBuff = StringBuilder()
        for (i in bytes.indices) {
            if (Integer.toHexString(0xFF and bytes[i].toInt()).length == 1) {
                md5StrBuff.append("0").append(Integer.toHexString(0xFF and bytes[i].toInt()))
            } else {
                md5StrBuff.append(Integer.toHexString(0xFF and bytes[i].toInt()))
            }
            if (bytes.size - 1 != i) {
                md5StrBuff.append(":")
            }
        }
        return md5StrBuff.toString()
    }

    init {
        System.loadLibrary("native-lib")
    }
}
