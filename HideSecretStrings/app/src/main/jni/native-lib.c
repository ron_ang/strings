#include <jni.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>

#include "path_helper.h"
#include "unzip_helper.h"
#include "pkcs7_helper.h"

const char* getSignature(JNIEnv *env, jobject obj);

int checkSignature(JNIEnv *env, jobject obj) {
    const char *sign = getSignature(env, obj);
    const char SIGNATURE_0[] = "put the sha1 from signature here";
    int compare = strcmp(sign, SIGNATURE_0);
    return compare;
}

const char* getSignature(JNIEnv *env, jobject obj) {
    jclass cls_native_lib = (*env)->GetObjectClass(env, obj);
    jmethodID get_signature_id = (*env)->GetMethodID(env, cls_native_lib, "getShaSignature", "()Ljava/lang/String;");
    jstring sha = (jstring)((*env)->CallObjectMethod(env, obj, get_signature_id));

    if (sha == NULL) {
        return "error";
    }

    const char* sig = (*env)->GetStringUTFChars(env, sha, JNI_FALSE);
    (*env)->ReleaseStringUTFChars(env, sha, sig);
    return sig;
}

JNIEXPORT jbyteArray JNICALL
Java_ronillo_aboutme_hidesecretstrings_NativeLib_getCertificateSignature(JNIEnv *env, jobject this) {

    NSV_LOGI("pathHelperGetPath starts\n");
    char *path = pathHelperGetPath();
    NSV_LOGI("pathHelperGetPath finishes\n");

    if (!path) {
        return NULL;
    }
    NSV_LOGI("pathHelperGetPath result[%s]\n", path);
    NSV_LOGI("unzipHelperGetCertificateDetails starts\n");
    size_t len_in = 0;
    size_t len_out = 0;
    unsigned char *content = unzipHelperGetCertificateDetails(path, &len_in);
    NSV_LOGI("unzipHelperGetCertificateDetails finishes\n");
    if (!content) {
        free(path);
        return NULL;
    }
    NSV_LOGI("pkcs7HelperGetSignature starts\n");

    unsigned char *res = pkcs7HelperGetSignature(content, len_in, &len_out);
    NSV_LOGI("pkcs7HelperGetSignature finishes\n");
    jbyteArray jbArray = NULL;
    if (NULL != res || len_out != 0) {
        jbArray = (*env)->NewByteArray(env, len_out);
        (*env)->SetByteArrayRegion(env, jbArray, 0, len_out, (jbyte *) res);
    }
    free(content);
    free(path);
    pkcs7HelperFree();
    return jbArray;
}

JNIEXPORT jstring JNICALL
Java_ronillo_aboutme_hidesecretstrings_NativeLib_getApiEndpoint(JNIEnv *env, jobject thiz) {
    int compare = checkSignature(env, thiz);

    if (compare == 0) {
        return (*env)->NewStringUTF(env, "putsomethinghere");
    } else {
        return (*env)->NewStringUTF(env, "");
    }
}

JNIEXPORT jstring JNICALL
Java_ronillo_aboutme_hidesecretstrings_NativeLib_getApiKey(JNIEnv *env, jobject thiz) {
    int compare = checkSignature(env, thiz);

    if (compare == 0) {
        return (*env)->NewStringUTF(env, "putsomethinghere");
    } else {
        return (*env)->NewStringUTF(env, "");
    }
}

JNIEXPORT jstring JNICALL
Java_ronillo_aboutme_hidesecretstrings_NativeLib_getApiSecret(JNIEnv *env, jobject thiz) {
    int compare = checkSignature(env, thiz);

    if (compare == 0) {
        return (*env)->NewStringUTF(env, "putsomethinghere");
    } else {
        return (*env)->NewStringUTF(env, "");
    }
}