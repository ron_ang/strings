# How to hide api secrets and to prevent tampering signatures of APK-files using NDK.


It may be helpful only for  non-rooted devices (and of course reverse-engineer always wins).

This example was created to help developers know how to hide api secrets from reverse engineering and stand against one type of an attack. The attack inserts code into Application class (or creates it), hooks the getPackageInfo method of the PackageManager then returns an original signature on demand. 

This attack is implemented in `'nkstool'` tool.
After user has applied `'nkstool'` tool on your APK, the application always gets its own signature in Java layer or through JNI when it's requested in APK even though it is not true.

The source code of this attacking tool can be found here:
https://github.com/L-JINBIN/ApkSignatureKiller


#### Used libraries and projects:

 * [Minizip](https://github.com/nmoinvaz/minizip) This program is distributed under the terms of the same license as zlib.
 * [https://github.com/W-WTerDan/pkcs7](https://github.com/W-WTerDan/pkcs7) Public domain


### Learn more

* [Hide Strings Using NDK/JNI from Reverse Engineering](https://ronillo-ang.blogspot.com/2020/11/hide-strings-using-ndk-jni-from-reverse.html)


### Thanks
    
 *  Guys who post their tools. I was inspired by [this one](https://github.com/DimaKoz/stunning-signature).


#### Licence

MIT License

Copyright (c) 2020 Ronillo Ang <https://about.me/ronillo>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
